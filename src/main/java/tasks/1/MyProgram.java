package first_program;

import java.util.Scanner;

public class MyProgram {
    public static void main(String[] args) {

        System.out.println("Введите число:");
        Scanner sc = new Scanner(System.in);
        long k = sc.nextLong();
        int max = 0;

        while (k > 0) {
            if (k % 10 > max) max = (int) (k % 10);
            k/=10;
        }
        System.out.println("Max: " + max);
    }

}
